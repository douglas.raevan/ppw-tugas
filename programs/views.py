from django.shortcuts import render
from .models import Program
from submitDonasi.models import submitDonasi

# Create your views here.
def index(request):
    query_results = Program.objects.all()
    return render(request, 'index.html', {'query_results': query_results})

def program_detail(request, pk):
    program = Program.objects.get(pk=pk)
    list_donatur = submitDonasi.objects.filter(program_id=pk)
    return render(request, 'program-detail.html', {'program': program, 'list_donatur': list_donatur})
