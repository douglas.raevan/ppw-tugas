from django.test import TestCase, Client
from django.urls import resolve
from .models import Program
from .views import index

# Create your tests here.

class ProgramPageTest(TestCase):

    def setUp(self):
        self.client = Client()

    def test_program_page_http_response(self):
        page = self.client.get('/programs/')
        self.assertEqual(page.status_code, 200)

    def test_program_page_programs_template(self):
        page = self.client.get('/programs/')
        self.assertTemplateUsed(page, 'index.html')

    def test_program_page_contains_all_programs(self):
        prog_id = '0001-test'
        Program.objects.create(program_id=prog_id, name='name', description='desc', detail='detail')
        page_content = self.client.get('/programs/').content.decode('utf8')
        self.assertIn(prog_id, page_content)
    
    def test_programpage_using_index_func(self):
        found = resolve('/programs/')
        self.assertEqual(found.func, index)

    def test_detailed_program_page(self):
        prog_id = '0002-test'
        Program.objects.create(program_id=prog_id, name='name', description='desc', detail='detail')
        page = self.client.get('/programs/' + prog_id + '/')
        self.assertEqual(page.status_code, 200)

    def test_program_model_string(self):
        prog_id = '0003-test'
        Program.objects.create(program_id=prog_id, name='name', description='desc', detail='detail')
        prog_str = str(Program.objects.get(program_id=prog_id))
        self.assertEqual(prog_id, prog_str)


    
    
        
    