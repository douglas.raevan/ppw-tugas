from django.db import models
from submitDonasi.models import submitDonasi
from datetime import datetime

# Create your models here.
class Program(models.Model):
    program_id = models.CharField(max_length=60, primary_key=True)
    name = models.CharField(max_length=60)
    time_published = models.DateTimeField(default=datetime.now)
    description = models.TextField(max_length=300)
    detail = models.TextField()
    target_donasi = models.IntegerField(default=1000000)
    news_tag = models.CharField(max_length=30)
    img_url = models.TextField(default="")

    def __str__(self):
        return self.program_id

    def hitung_donasi(self):
        donasi_query = submitDonasi.objects.filter(program_id=self.program_id)
        res = 0
        for q in donasi_query:
            res += q.donasi
        return res
        
    def percentage(self):
        return self.hitung_donasi()*100 // self.target_donasi

    def banyak_donasi(self):
        return submitDonasi.objects.filter(program_id=self.program_id).count()
