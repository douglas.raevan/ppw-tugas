from django.db import models
from django.utils.text import slugify

# Create your models here.

class News(models.Model):
	class Meta:
		verbose_name_plural = "news"

	def __str__(self):
		return ("News: " + self.title)
	title = models.CharField(max_length=140)
	content = models.TextField()
	content_short = models.TextField(default="will-be-filled-by-system")
	image_src = models.TextField()
	news_tag = models.CharField(max_length=30)
	slug = models.SlugField(default="will-be-filled-by-system", max_length=140)


	def save(self, *args, **kwargs):
		self.slug = slugify(self.title)[0:50]
		tmp = self.content.split(" ")[:45]
		tmp2 = ""
		for word in tmp:
			tmp2 = tmp2 + word + " "
		self.content_short = tmp2
		super(News, self).save(*args, **kwargs)