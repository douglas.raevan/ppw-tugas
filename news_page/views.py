from django.shortcuts import render
from django.http import HttpResponse

from .models import News
from programs.models import Program

# Create your views here.
def newspage(request):
	my_news = News.objects.all()

	return render(request, 'news.html', {'my_news' : my_news})

def detailed_newspage(request, id, slug):
	my_news = News.objects.get(id=id)
	if (my_news.slug != slug):
		return HttpResponse("404 Not Found")

	my_news_programs = Program.objects.filter(news_tag=my_news.news_tag)

	return render(request, 'detailed_newspage.html', {'my_news' : my_news, 'my_news_programs' : my_news_programs})