from django.test import TestCase, RequestFactory
from django.urls import resolve
from django.utils.text import slugify

from .views import *
from .models import *

# Create your tests here.
"""
# Orang mengunjungi halaman news
# Orang melihat terdapat berita-berita
# Kemudian orang dapat klik baca lebih lanjut
# Kemudian akan muncul halaman berita lengkap
# Akan muncul daftar program yang tersedia
# Orang dapat klik untuk melakukan menuju halaman program (Douglas)
# Orang dapat klik DONASI SEKARANG (Adiva)
"""

class NewsPageURLsTest(TestCase):
	def test_news_url_will_fire_newspage_views(self):
		"""
		Test that ./news/ will fire
		newspage function from views.
		"""
		root = resolve('/news/')
		self.assertEqual(root.func, newspage)

class NewspageViewsTest(TestCase):
	def setUp(self):
		self.factory = RequestFactory()

	def test_newspage_function_use_correct_template_and_models(self):
		"""
		Test to make sure that when newspage function
		is called, it is using the correct template
		(news.html)
		"""
		request = self.factory.get('/news/')
		response = newspage(request) # This also test the existence of the models.
		self.assertEqual(response.status_code, 200)

		"""
		Test to make sure newspage function
		use news.html template
		"""
		with self.assertTemplateUsed('news.html'):
			response = newspage(request)

class DetailedNewspageViewsTest(TestCase):
	def setUp(self):
		self.news = News.objects.create(title="Test1", content="Testing the first news", image_src="http://bdfjade.com/data/out/139/6414552-cat-image.jpg")
		self.factory = RequestFactory()

	def test_detailed_newspage_function_use_correct_template(self):
		"""
		Test to make sure that when detailed_newspage function
		is called, it is using the correct template
		(detailed_newspage.html)
		"""
		request = self.factory.get('/news/' + str(self.news.id) + '/' + self.news.slug)
		response = detailed_newspage(request, self.news.id, self.news.slug)
		self.assertEqual(response.status_code, 200)

		"""
		Test to make sure newspage function
		use news.html template
		"""
		with self.assertTemplateUsed('detailed_newspage.html'):
			response = detailed_newspage(request, self.news.id, self.news.slug)

	def test_detailed_newspage_news_not_found(self):
		"""
		Test to make sure the function detailed_newspage return a 404 Not Found
		HttpResponse (status code 200) when the news isn't found.
		"""
		request = self.factory.get('/news/132/random/')
		response = detailed_newspage(request, 1, 'random')
		self.assertEqual(response.status_code, 200)



class NewsModelsTest(TestCase):
	def setUp(self):
		self.news = News.objects.create(title="Test1", content="Testing the first news", image_src="http://bdfjade.com/data/out/139/6414552-cat-image.jpg")

	def test_create_news_models_object(self):
		"""
		Test to make sure we can put an object
		into the models' database, and retrieve
		the correct one.
		"""
		tmp = self.news.content.split(" ")[:45]
		tmp2 = ""
		for word in tmp:
			tmp2 = tmp2 + word + " "

		news_retrieve = News.objects.get(title="Test1")
		self.assertEqual(news_retrieve.title, self.news.title)
		self.assertEqual(news_retrieve.content, self.news.content)
		self.assertEqual(news_retrieve.content_short, tmp2)
		self.assertEqual(news_retrieve.image_src, self.news.image_src)
		self.assertEqual(news_retrieve.slug, slugify(self.news.title))

	def test_object_naming_in_database(self):
		"""
		Test to make sure the object is named
		'News: [NEWS TITLE]' in the database.
		"""
		self.assertEqual(self.news.__str__(), "News: " + self.news.title)




