# ppw-tugas
# Kelas F - Kelompok 10

Nama Anggota:
- Adiva Reyhan Puteri (NPM: 1706984505)
- Douglas Raevan Faisal (NPM: 1706984562)
- Muhammad Khatami (NPM: 1706044055)
- Sandika Prangga Putra (NPM: 1706043802)

Status Pipelines:
[![pipeline status](https://gitlab.com/douglas.raevan/ppw-tugas/badges/master/pipeline.svg)](https://gitlab.com/douglas.raevan/ppw-tugas/commits/master)

Coverage Report:
[![coverage report](https://gitlab.com/douglas.raevan/ppw-tugas/badges/master/coverage.svg)](https://gitlab.com/douglas.raevan/ppw-tugas/commits/master)

Heroku App Link:
http://ppw-f-kelompok10.herokuapp.com/