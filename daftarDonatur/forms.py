from django import forms
from .models import User
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator

class UserForm(forms.Form):

	fullname_attrs = {

		'class':'form-control',
		'type':'text',
		'placeholder':'nama lengkap anda',
	}

	birthdate_attrs = {

		'class':'form-control',
		'type':'date',
		'placeholder':'pilih tanggal',
	}

	email_attrs = {

		'class':'form-control',
		'type':'email',
		'placeholder':'email anda',
	}

	password_attrs={

		'class':'form-control',
		'type':'password',
		'placeholder':'kata sandi',
	}

	fullname = forms.CharField(label = "Nama Lengkap", max_length = 30, widget = forms.TextInput(attrs = fullname_attrs))
	birthdate = forms.DateField(label = "Tanggal Lahir", widget = forms.DateInput(attrs = birthdate_attrs))
	email=forms.EmailField(label="Email", max_length=30, widget=forms.EmailInput(attrs=email_attrs))
	password=forms.CharField(label="Password", max_length=8, min_length=8, validators=[RegexValidator('^(\w+\d+|\d+\w+)+$', message="Kata sandi harus alfanumerik")], widget=forms.PasswordInput(attrs=password_attrs))





