"""lab6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from django.contrib.auth import views
from django.conf.urls import include
from .views import daftar
from django.conf import settings


urlpatterns = [
    path('',daftar, name='daftar'), #ini halaman daftar
    # path('konfirm/', konfirm, name="konfirm"), #ini konfirmasi udh daftar
    # path('login/',  views.LoginView.as_view(), name="login"),
    # path('logout/', logoutPage,  name ='logout'),
    path('logout/', views.LogoutView.as_view(),  {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),

]
