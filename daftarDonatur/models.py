from django.db import models
from datetime import date

# Create your models here.
class User(models.Model):
	fullname=models.CharField(max_length=30)
	birthdate=models.DateField()
	email=models.EmailField(max_length=30, primary_key=True)
	password=models.CharField(max_length=10)

def __str__(self):
	return self.fullname