from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
import unittest

# Create your tests here.
class daftarDonaturTest(TestCase):

	def test_daftar_url_exist(self):
		response = Client().get('/masuk/')
		self.assertEqual(response.status_code, 200)

	def test_daftar_using_index_func(self):
		found = resolve('/masuk/')
		self.assertEqual(found.func, daftar)

	# def test_model_can_creat_new_user(self):
	# 	new_user = User.objects.create(fullname="me", birthdate="2018-01-01", email="itsme@yahoo.com", password="hello")

	# 	counting_all_user = User.objects.all().count()
	# 	self.assertEqual(counting_all_user, 1)

	# def test_konfirm_url_exist(self):
	# 	response = Client().get('/daftar/konfirm/')
	# 	self.assertEqual(response.status_code, 200)

	# def test_konfirm_using_index_func(self):
	# 	found=resolve('/masuk/konfirm/')
	# 	self.assertEqual(found.func, konfirm)

	# def test_konfrim_html_message(self):
	# 	request=HttpRequest()
	# 	response=konfirm(request)
	# 	html_response=response.content.decode('UTF8')
	# 	self.assertIn('ANDA TELAH TERDAFTAR', html_response)