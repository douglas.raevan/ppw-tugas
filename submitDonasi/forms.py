from django import forms
from .models import submitDonasi
from django.forms import ModelForm

class donasiForm(forms.ModelForm):

    '''def __init__(self, prog):
        super().__init__()
        self.program_id = forms.HiddenInput(attrs={'value': prog})
    '''
    CATEGORY_CHOICES = (
        ('YES', 'yes'),
        ('NO', 'no')
    )
    class Meta:

        model = submitDonasi
        fields = ['donasi']

        widgets = {
            'donasi': forms.NumberInput(attrs={'class': 'form-control ', 'type': 'number', 'placeholder': 'jumlah donasi anda'}),
            #'anonym': forms.Select(attrs={'class': 'form-control', 'type': 'text', 'placeholder': 'yes or no'}),
            #'program_id': forms.HiddenInput(attrs={'value': program})

        }

        labels = {
            'donasi': 'Masukan jumlah Donasi anda',
            'anonym': 'Apakah anda ingin berdonasi secara anonim'
        }


    anonym = forms.ChoiceField(choices=CATEGORY_CHOICES, label="Apakah Anda ingin berdonasi secara anonim?", initial='', widget=forms.Select(attrs={'class': 'form-control', 'type': 'text', 'placeholder': 'yes or no'}))
