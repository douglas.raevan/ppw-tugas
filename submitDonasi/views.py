from django.shortcuts import render
from .forms import donasiForm
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from programs.models import Program
from .models import submitDonasi as Donasi
from daftarDonatur.models import User
from daftarDonatur.views import daftar
from django.core import serializers
import requests
import json
from django.http import HttpResponse, JsonResponse

# Create your views here.
def halaman_form(request, program):
    prog = Program.objects.get(pk=program)
    donateform = donasiForm()
    program_id = program
    program_name = prog.name
    print(program_name)
    jumlah=sumDonasi(program)
    if request.user.is_authenticated:
        return render(request, 'submitDonasi.html',
        {'form': donateform,'program_name': program_name,
        'program_id': program_id, 'sum': jumlah, 'program': prog})
    else:
        return HttpResponseRedirect('/masuk/')

def submitDonasi(request):
    if request.method == "POST":
        form = donasiForm(request.POST)
        if form.is_valid():
            response = {}
            response['donasi'] = request.POST['donasi']
            response['username'] = request.POST['username']
            response['anonym'] = request.POST['anonym']
            response['program_id'] = request.POST['program_id']
            response['program_name'] = request.POST['program_name']
            donasi = Donasi(donasi=response['donasi'],
                                    username=response['username'],
                                    anonym=response['anonym'],
                                    program_id=response['program_id'],
                                    program_name=response['program_name'])
            donasi.save()
            return render(request, 'thanks.html', {})
    else:
        form = donasiForm()
    return halaman_form(request, request.POST['program_id'])

def sumDonasi(program_id):
    listDonasi = Donasi.objects.filter(program_id=program_id)
    sum = 0
    for i in listDonasi:
        sum += i.donasi
    return sum

def indexHistory(request):
    return render(request, 'history.html')

def userHistory(request):
    response = {}
    if request.user.is_authenticated:
        user = request.user
        #print(user.first_name)
        response['username'] = user.username
        histori_donasi = Donasi.objects.filter(username=response['username'])
        return HttpResponse(serializers.serialize('json', histori_donasi),
        content_type='application/json')
    else:
        return HttpResponseRedirect('/masuk/')
