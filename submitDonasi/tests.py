from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import submitDonasi
from programs.models import Program
from daftarDonatur.models import User
import unittest

# Create your tests here.

class submitDonasiPageTest(TestCase):

    def test_history_donasi_url_is_exist(self):
        response = Client().get('/submitDonasi/history/')
        self.assertEqual(response.status_code, 200)

    def setUp(self):
        self.client = Client()
        self.prog_id = '0003-test'
        Program.objects.create(program_id=self.prog_id, name='name', description='desc', detail='detail')

    def test_list_history_data_url_is_exist(self):
        response = Client().get('/submitDonasi/history/listHistory/')
        self.assertNotEqual(response.status_code, 200)

    #karena halaman submit donasi tidak dapat di akses kecuali melalui 'donasi'
    def test_submitDonasi_page_http_response(self):
        page=self.client.get('/submitDonasi/'+ self.prog_id +'/')
        self.assertNotEqual(page.status_code, 200)

    def test_submitDonasi_can_submit_new_donation(self):
        new_user = User.objects.create(fullname="me", birthdate="2018-01-01", email="itsme@yahoo.com", password="hello")
        new_donation = submitDonasi.objects.create(donasi=100000, anonym='YES', program_id=self.prog_id)
        self.assertEqual(Program.objects.get(program_id=self.prog_id).hitung_donasi(), 100000)
