from django.db import models
from django.forms import forms
from django.core.validators import MinValueValidator, MaxValueValidator
# Create your models here.

class submitDonasi(models.Model):

    CATEGORY_CHOICES = (
        ('YES', 'YES'),
        ('NO', 'NO')
    )

    donasi = models.IntegerField(validators=[MinValueValidator(0)])
    username = models.CharField(max_length=30, default="none")
    #email = models.EmailField(max_length=30)
    #password = models.CharField(max_length=30)
    anonym = models.CharField(max_length=3, choices=CATEGORY_CHOICES)
    program_id = models.CharField(max_length=60)
    program_name = models.CharField(max_length=300, default="program_name")
