from django.urls import path
from submitDonasi import views

urlpatterns = [
	path('submit-donasi/', views.submitDonasi, name = 'submitDonasi'),
	path('history/', views.indexHistory, name = 'history'),
	path('history/listHistory/', views.userHistory, name = 'listHistory'),
	path('<str:program>/', views.halaman_form, name = 'donasiForm'),
]
