from django.shortcuts import render
from django.http import HttpResponse
from programs.models import Program
from daftarDonatur.models import User
from news_page.models import News

# Create your views here.
def index(request):
    programs = Program.objects.all()
    user = User.objects.all()
    jumlah_donasi = 0
    for i in programs:
        jumlah_donasi += i.hitung_donasi()
    news = News.objects.all()
    return render(request, "landingPage.html", {'banyak_program': programs.count(), 'jumlah_donasi': jumlah_donasi, 'news' : news, 'semua_program' : programs, 'banyak_donatur' : user.count()})
