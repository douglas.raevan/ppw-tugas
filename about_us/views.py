import json
import requests

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from django.contrib.staticfiles.templatetags.staticfiles import static

from .models import Testimony
from .forms import TestimonyForm

# Create your views here.
def index(request):
	form = TestimonyForm()
	return render(request, 'about-us.html', {'form': form})

def view_testimonies(request):
	tests = list(Testimony.objects.all())
	data = serializers.serialize('json', tests)
	return HttpResponse(data, content_type='application/json')

def add_testimony(request, username, test):
	print("Username:", username)
	print("Test:", test)
	test = Testimony(username=username, test=test)
	test.save()
	return HttpResponse("Success")
	'''
	if (request,method == 'POST'):
		form = TestimonyForm(request.POST)
		if form.is_valid():
			response = {}
			response['test'] = request.POST['test']
			response['username'] = request.POST['username']
			test = Testimony(username=response['username'], test=response['test'])
			test.save()

			return HttpResponse("Success")
	return HttpResponse("Fail")'''