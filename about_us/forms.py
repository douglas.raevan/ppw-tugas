from django import forms

from .models import Testimony

class TestimonyForm(forms.ModelForm):
    
    class Meta:
        model = Testimony
        fields = ('test',)
        widgets = {
            'test': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Testimoni Anda'}),
        }

        labels = {
        	'test': 'Testimoni',
        }