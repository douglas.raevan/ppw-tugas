from django.test import TestCase, Client
from django.urls import resolve
from .models import Testimony
from .views import index

# Create your tests here.
class AboutUsPageTest(TestCase):

	def setUp(self):
		self.client = Client()

	def test_about_us_page_http_response(self):
		page = self.client.get('/profil/')
		self.assertEqual(page.status_code, 200)

	def test_about_us_page_template(self):
		page = self.client.get('/profil/')
		#print(type(page))
		self.assertTemplateUsed(page, 'about-us.html')

    
	def test_about_us_page_using_index_func(self):
	    found = resolve('/profil/')
	    self.assertEqual(found.func, index)


	def test_json_return(self):
	    create = Testimony.objects.create(username='john', test='testimony')
	    #print(create)
	    page = self.client.get('/profil/view/').content.decode('utf8')
	    self.assertIn('john', page)

'''
    def test_program_model_string(self):
        prog_id = '0003-test'
        Program.objects.create(program_id=prog_id, name='name', description='desc', detail='detail')
        prog_str = str(Program.objects.get(program_id=prog_id))
        self.assertEqual(prog_id, prog_str)'''